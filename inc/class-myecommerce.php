<?php
/**
 * MyEcommerce Class
 *
 * @author   MI Dexigner
 * @since    1.0
 */
 if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( 'MyEcommerce' ) ) { 
class MyEcommerce{
	public function __construct() {
		//add_action( 'wp_enqueue_scripts', array( $this,'sf_child_theme_dequeue_style'), 999 );
		add_action( 'after_setup_theme', array( $this,'theme_setup'), 20 );
		add_action( 'wp_enqueue_scripts', array( $this,'enqueue_child_styles'), 20 );
		add_action( 'widgets_init', array( $this,'unregister_a_widget'), 11 );
		
		
	}
	

/**
 * Dequeue the Storefront Parent theme core CSS
 */
public function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}

/**
* Sets up theme defaults and registers support for various WordPress features.
*/
public function theme_setup(){
remove_theme_support( 'align-wide' );	
}

/**
 * Enqueue Storefront Styles
*/
	public function enqueue_styles() {
		global $storefront_version;

		wp_enqueue_style( 'storefront-style', get_template_directory_uri() . '/style.css', $storefront_version );
	}
	
	/**
	 * Enqueue Storechild Styles
	 * @return void
	 */
	public function enqueue_child_styles() {
		global $storefront_version, $myEcommerce_version;

		/**
		 * Styles
		 */
		wp_style_add_data( 'storefront-child-style', 'rtl', 'replace' );

		wp_enqueue_style( 'lato', '//fonts.googleapis.com/css?family=Lato:400,700,400italic', array( 'storefront-style' ) );
		wp_enqueue_style( 'playfair-display', '//fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic', array( 'storefront-style' ) );
	}
	
/**
	 * Unregister Sidebar Widget Storechild 
	 * @return void
	 */	
public function unregister_a_widget() {

    unregister_sidebar( 'header-1' );

}
	

}

return new MyEcommerce();
}

?>