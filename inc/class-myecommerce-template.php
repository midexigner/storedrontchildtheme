<?php
/**
 * MyEcommerce_Template Class
 *
 * @author   MI Dexigner
 * @since    1.0
 */

if ( ! class_exists( 'MyEcommerce_Template' ) ) {
class MyEcommerce_Template{
/**
* Setup class.
*
* @since 1.0
*/
public function __construct() {
	add_action( 'storefront_header', array( $this, 'primary_navigation_wrapper' ),45 );
	add_action( 'storefront_header', array( $this, 'primary_navigation_wrapper_close' ), 65 );

	add_action( 'init',array( $this, 'custom_storefront_markup' ));
	add_action( 'init', array( $this, 'remove_homepage_templates' ));
	add_action( 'storefront_before_content',array( $this, 'new_homepage_sections' ),10 );

	add_filter( 'storefront_recent_products_args',array( $this, 'product_columns_three' ),99 );
	add_filter( 'storefront_popular_products_args',array( $this, 'product_columns_three' ),99 );
	add_filter( 'storefront_on_sale_products_args',array( $this, 'product_columns_three' ),99 );
	add_filter( 'storefront_best_selling_products_args', array( $this, 'product_columns_three' ),99 );
	
	add_action( 'init', array( $this, 'remove_section' ));
	add_action( 'init', array( $this, 'add_section' ));
	
		
	//add_filter( 'woocommerce_product_single_add_to_cart_text', array( $this, 'wc_custom_single_addtocart_text') );
	add_filter( 'woocommerce_product_add_to_cart_text', array( $this, 'bryce_archive_add_to_cart_text') );
	
	
			
}
		/**
		 * Primary navigation wrapper
		 * @return void
		 */
		public function primary_navigation_wrapper() {
			echo '<section class="myEcommerce-primary-navigation">';
		}
		/**
		 * Primary navigation wrapper close
		 * @return void
		 */
		public function primary_navigation_wrapper_close() {
			echo '</section>';
		}

		/**
		 * Custom markup tweaks
		 * @return void
		 */
		public function custom_storefront_markup() {
			global $storefront_version;
			if ( version_compare( $storefront_version, '2.3.0', '>=' ) ) {
			if ( storefront_is_woocommerce_activated() ) {
			add_filter( 'woocommerce_breadcrumb_defaults', array( $this, 'change_breadcrumb_delimiter' ), 15 );
			remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
			add_action( 'storefront_content_top', 'woocommerce_breadcrumb', 10 );
				}
			}
		}
/**
 * Remove homepage sections from default location
 *
 * @return void
*/
		public function remove_homepage_templates() {
			remove_action( 'homepage', 'storefront_featured_products', 40 );
			remove_action( 'homepage', 'storefront_featured_products', 40 );
			remove_action( 'homepage', 'storefront_featured_products', 40 );
			remove_action( 'homepage', 'storefront_featured_products', 40 );
			remove_action( 'homepage', 'storefront_homepage_content',  10 );
		}

/**
		 * Add homepage sections to new location
		 *
		 * @return void
		 */
		public function new_homepage_sections() {
			
			ob_start();
			require_once('template/slider.php');
			
			return ob_get_clean();
			
			
		}
		
		/**
		 * Return args to set product display limit and column amount to 3
		 * @param  array $args args passed to the filter
		 * @return array       the modified args
		 */
		public function product_columns_three( $args ) {
			$args['limit'] 		= 3;
			$args['columns'] 	= 3;
			return $args;
		}	

/**
		 * Remove homepage sections from default location
		 * Remove the breadcrumb delimiter
		 * @param  array $defaults The breadcrumb defaults
		 * @return array           The breadcrumb defaults
		 */
		public function change_breadcrumb_delimiter( $defaults ) {
			$defaults['wrap_before'] = '<nav class="woocommerce-breadcrumb">';
			$defaults['wrap_after']  = '</nav>';
			return $defaults;
		}		
/*
Remove  section

*/
public function remove_section(){
	/* Before site*/
	remove_action( 'storefront_before_site', array( 'Storefront_Visual_Guide', 'display_filters' ), 1);
	/* header */
	remove_action( 'storefront_header', 'storefront_header_container', 0);
remove_action( 'storefront_header', 'storefront_skip_links', 5);
remove_action( 'storefront_header', 'storefront_site_branding', 20);
remove_action( 'storefront_header', 'storefront_secondary_navigation', 30);
remove_action( 'storefront_header', 'storefront_product_search', 40);
remove_action( 'storefront_header', 'storefront_header_container_close', 41);
remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper', 42);
remove_action( 'storefront_header', array( 'MyEcommerce_Template', 'primary_navigation_wrapper' ), 45);
remove_action( 'storefront_header', 'storefront_primary_navigation', 50);
remove_action( 'storefront_header', 'storefront_header_cart', 60);
remove_action( 'storefront_header', array( 'MyEcommerce_Template', 'primary_navigation_wrapper_close' ), 65);
remove_action( 'storefront_header', 'storefront_primary_navigation_wrapper_close', 68);
	/* Home section */
	remove_action( 'homepage', 'storefront_product_categories', 20);
	remove_action( 'homepage', 'storefront_recent_products', 30);
	remove_action( 'homepage', 'storefront_popular_products', 50);
	remove_action( 'homepage', 'storefront_on_sale_products', 60);
	remove_action( 'homepage', 'storefront_best_selling_products', 70);
	
	/* footer */
	remove_action( 'storefront_footer', 'storefront_credit', 20);
	
	remove_action( 'storefront_footer', 'storefront_footer_widgets', 10);
	remove_action( 'storefront_footer', 'storefront_handheld_footer_bar', 999);
	remove_action( 'storefront_after_footer', 'storefront_sticky_single_add_to_cart', 999);

	remove_action( 'woocommerce_before_shop_loop', 'storefront_woocommerce_pagination', 30 );
}

public function add_section(){
	/* header */
	add_action( 'storefront_header', 'storefront_header_container', 0);
	add_action( 'storefront_header', 'storefront_site_branding', 20);
	add_action( 'storefront_header', 'storefront_secondary_navigation', 30);
	//add_action( 'storefront_header', 'primary_navigation_wrapper', 45);
	//add_action( 'storefront_header', 'storefront_primary_navigation', 50);
	//add_action( 'storefront_header', 'primary_navigation_wrapper_close', 65);
	
}
function wc_custom_single_addtocart_text() {
    return "Pay Now";
}

function bryce_archive_add_to_cart_text() {
        return __( 'Pay Now', 'your-slug' );
}
	
	
}

return new MyEcommerce_Template();
}