<?php
//https://www.quora.com/Is-it-possible-to-have-more-than-one-checkout-page-for-different-products-product-categories-on-a-WordPress-website

//https://echo5digital.com/blog/how-to-add-images-to-your-woocommerce-product-addons/
/**
 * Set the theme version number as a global variable
 */
$theme				= wp_get_theme( 'myEcommerce' );
$myEcommerce_version	= $theme['Version'];

$theme				= wp_get_theme( 'storefront' );
$storefront_version	= $theme['Version'];

/**
 * Load the individual classes required by this theme
 */
require_once( 'inc/class-myecommerce.php' );
require_once( 'inc/class-myecommerce-template.php' );

/**
 * Display the custom text field
 * @since 1.0.0
 */
function cfwc_create_custom_field() {
 $args = array(
 'id' => 'custom_text_field_title',
 'label' => __( 'Custom Text Field Title', 'cfwc' ),
 'class' => 'cfwc-custom-field',
 'desc_tip' => true,
 'description' => __( 'Enter the title of your custom text field.', 'ctwc' ),
 );
 woocommerce_wp_text_input( $args );
// woocommerce_wp_hidden_input()
// woocommerce_wp_textarea_input()
 // woocommerce_wp_checkbox()
}
add_action( 'woocommerce_product_options_general_product_data', 'cfwc_create_custom_field' );

/**
 * Save the custom field
 * @since 1.0.0
 */
function cfwc_save_custom_field( $post_id ) {
 $product = wc_get_product( $post_id );
 $title = isset( $_POST['custom_text_field_title'] ) ? $_POST['custom_text_field_title'] : '';
 $product->update_meta_data( 'custom_text_field_title', sanitize_text_field( $title ) );
 $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field' );

/**
 * Display custom field on the front end
 * @since 1.0.0
 */
function cfwc_display_custom_field() {
 global $post;
 // Check for the custom field value
 $product = wc_get_product( $post->ID );
 $title = $product->get_meta( 'custom_text_field_title' );
 if( $title ) {
 // Only display our field if we've got a value for the field title
 printf(
 '<div class="cfwc-custom-field-wrapper"><label for="cfwc-title-field">%s</label><input type="text" id="cfwc-title-field" name="cfwc-title-field" value=""></div>',
 esc_html( $title )
 );
 }
}
add_action( 'woocommerce_before_add_to_cart_button', 'cfwc_display_custom_field' );

/**
 * Validate the text field
 * @since 1.0.0
 * @param Array 		$passed					Validation status.
 * @param Integer   $product_id     Product ID.
 * @param Boolean  	$quantity   		Quantity
 */
function cfwc_validate_custom_field( $passed, $product_id, $quantity ) {
 if( empty( $_POST['cfwc-title-field'] ) ) {
 // Fails validation
 $passed = false;
 wc_add_notice( __( 'Please enter a value into the text field', 'cfwc' ), 'error' );
 }
 return $passed;
}
//add_filter( 'woocommerce_add_to_cart_validation', 'cfwc_validate_custom_field', 10, 3 );

/**
 * Add the text field as item data to the cart object
 * @since 1.0.0
 * @param Array 		$cart_item_data Cart item meta data.
 * @param Integer   $product_id     Product ID.
 * @param Integer   $variation_id   Variation ID.
 * @param Boolean  	$quantity   		Quantity
 */
function cfwc_add_custom_field_item_data( $cart_item_data, $product_id, $variation_id, $quantity ) {
 if( ! empty( $_POST['cfwc-title-field'] ) ) {
 // Add the item data
 $cart_item_data['title_field'] = $_POST['cfwc-title-field'];
 $product = wc_get_product( $product_id ); // Expanded function
 $price = $product->get_price(); // Expanded function
 $cart_item_data['total_price'] = $price + 100; // Expanded function
 }
 return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'cfwc_add_custom_field_item_data', 10, 4 );
 /**
 * Update the price in the cart
 * @since 1.0.0
 */
function cfwc_before_calculate_totals( $cart_obj ) {
 if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
 return;
 }
 // Iterate through each cart item
 foreach( $cart_obj->get_cart() as $key=>$value ) {
 if( isset( $value['total_price'] ) ) {
 $price = $value['total_price'];
 $value['data']->set_price( ( $price ) );
 }
 }
}
add_action( 'woocommerce_before_calculate_totals', 'cfwc_before_calculate_totals', 10, 1 );

/**
 * Display the custom field value in the cart
 * @since 1.0.0
 */
function cfwc_cart_item_name( $name, $cart_item, $cart_item_key ) {
 if( isset( $cart_item['title_field'] ) ) {
 $name .= sprintf(
 '<p>%s</p>',
 esc_html( $cart_item['title_field'] )
 );
 }
 return $name;
}
add_filter( 'woocommerce_cart_item_name', 'cfwc_cart_item_name', 10, 3 );
/**
 * Add custom field to order object
 */
function cfwc_add_custom_data_to_order( $item, $cart_item_key, $values, $order ) {
 foreach( $item as $cart_item_key=>$values ) {
 if( isset( $values['title_field'] ) ) {
 $item->add_meta_data( __( 'Custom Field', 'cfwc' ), $values['title_field'], true );
 }
 }
}
add_action( 'woocommerce_checkout_create_order_line_item', 'cfwc_add_custom_data_to_order', 10, 4 );



/* custom order*/
/*function timing_is_locked(){
date_default_timezone_set('Europe/dusseldorf');
//echo date_default_timezone_get() . ' => ' . date('e') . ' => ' . date('T');
$t=time();
echo date("l jS \of F Y h:i:s A") . "<br>";
echo(date("Y-m-d",$t));
}
add_action( 'woocommerce_after_add_to_cart_button', 'timing_is_locked' );
*/

function _redirect_checkout_add_cart( $url ) {
    $url = get_permalink( get_option( 'woocommerce_checkout_page_id' ) ); 
    return $url;
}
 
add_filter( 'woocommerce_add_to_cart_redirect', '_redirect_checkout_add_cart' );

function _remove_default_gateway( $load_gateways ){
 
	unset( $load_gateways[0] ); // WC_Gateway_BACS
	unset( $load_gateways[1] ); // WC_Gateway_Cheque
	unset( $load_gateways[2] ); // WC_Gateway_COD (Cash on Delivery)
	unset( $load_gateways[3] ); // WC_Gateway_Paypal
 
	return $load_gateways;
}
 
add_filter( 'woocommerce_payment_gateways', '_remove_default_gateway', 10, 1 );
/*
 * if no methods are allowed, allow PayPal Payflow
 */
function _change_wc_gateway_if_empty( $allowed_gateways ){
 
	if( empty( $allowed_gateways ) ) {
		$allowed_gateways = array();
		$all_gateways = WC()->payment_gateways->payment_gateways();
		$allowed_gateways['paypal_pro_payflow'] = $all_gateways['paypal_pro_payflow'];
	}
 
	return $allowed_gateways;
 
}
//add_filter('woocommerce_available_payment_gateways','_change_wc_gateway_if_empty', 9999, 1 );

/**
 * Change the default state and country on the checkout page
 */
add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );
add_filter( 'default_checkout_billing_state', 'change_default_checkout_state' );


function change_default_checkout_state() {
  return 'NY'; // state code
}
/**
 * Change the default country on the checkout for non-existing users only
 */
add_filter( 'default_checkout_billing_country', 'change_default_checkout_country', 10, 1 );

function change_default_checkout_country( $country ) {
    // If the user already exists, don't override country
    if ( WC()->customer->get_is_paying_customer() ) {
        return $country;
    }

    return 'US'; // Override default to United State (an example)
}

// https://www.facebook.com/rudrastyh

/* woocomerce order button html hook*/
add_filter( 'woocommerce_order_button_html', '_custom_button_html' );
 
function _custom_button_html( $button_html ) {
	$button_html = str_replace( 'Place order', 'Proceed Now', $button_html );
	return $button_html;
}
add_filter( 'gettext', '_custom_paypal_button_text', 20, 3 );
 
function _custom_paypal_button_text( $translated_text, $text, $domain ) {
 
	if( $translated_text == 'Proceed to PayPal' ) {
		$translated_text = 'Pay with PayPal'; // new button text is here
	}
 
	return $translated_text;
}






	
// Disable Cart, Checkout, Add Cart

if(in_array(date('D'),['Mon','Sun'])) {
add_action ('init', 'bbloomer_woocommerce_holiday_mode');
}

 
function bbloomer_woocommerce_holiday_mode() {
   remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
   remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
   remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
   remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
   add_action( 'woocommerce_before_main_content', 'bbloomer_wc_shop_disabled', 5 );
   add_action( 'woocommerce_before_cart', 'bbloomer_wc_shop_disabled', 5 );
   add_action( 'woocommerce_before_checkout_form', 'bbloomer_wc_shop_disabled', 5 );
}
 
 
// Show Holiday Notice
 
function bbloomer_wc_shop_disabled() {
        wc_print_notice( 'Our Online Shop is Closed Today :)', 'error');
} 


if( !wp_next_scheduled( 'import_into_db' ) ) {
wp_schedule_event( time('1427488800'), 'daily', 'import_into_db' );

function import_into_db(){

////My code
echo "Hello World";

}
add_action('wp', 'import_into_db');

}
/* default city */
add_filter( 'woocommerce_checkout_fields', 'bbloomer_set_checkout_field_input_value_default' );
 
function bbloomer_set_checkout_field_input_value_default($fields) {
    $fields['billing']['billing_city']['default'] = 'London';
    return $fields;
}
// add to cart message disabled
add_filter( 'wc_add_to_cart_message_html', 'empty_wc_add_to_cart_message');
function empty_wc_add_to_cart_message( $message, $products ) { 
    return ''; 
};
add_action('woocommerce_before_cart','cartPage_check');
function cartPage_check(){
		if(is_page('cart')){
	wp_redirect(bloginfo('url'));
		echo "Hello";
	}
}

//add_action('woocommerce_add_to_cart_redirect','cartPage_check');

/**
 * Skip cart and redirect to direct checkout
 * 
 * @package WooCommerce
 * @version 1.0.0
 * @author Alessandro Benoit
 */
// Skip the cart and redirect to check out url when clicking on Add to cart
add_filter ( 'add_to_cart_redirect', 'redirect_to_checkout' );
function redirect_to_checkout() {
    
	global $woocommerce;
	// Remove the default `Added to cart` message
	wc_clear_notices();
	return $woocommerce->cart->get_checkout_url();
	
}
// Global redirect to check out when hitting cart page
add_action( 'template_redirect', 'redirect_to_checkout_if_cart' );
function redirect_to_checkout_if_cart() {
	
	if ( !is_cart() ) return;
	global $woocommerce;
    // Redirect to check out url
	wp_redirect( $woocommerce->cart->get_checkout_url(), '301' );
	exit;
	
}
// Empty cart each time you click on add cart to avoid multiple element selected
add_action( 'woocommerce_add_cart_item_data', 'clear_cart', 0 );
function clear_cart () {
	global $woocommerce;
	$woocommerce->cart->empty_cart();
}
// Edit default add_to_cart button text
add_filter( 'add_to_cart_text', 'woo_custom_cart_button_text' );
add_filter( 'woocommerce_product_single_add_to_cart_text', 'custom_cart_button_text' );
function custom_cart_button_text() {
	return __( 'Buy', 'woocommerce' );
}
// Unset all options related to the cart
update_option( 'woocommerce_cart_redirect_after_add', 'no' );
update_option( 'woocommerce_enable_ajax_add_to_cart', 'no' );

/**
 * Auto Complete all WooCommerce orders.
 */
add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
    if ( ! $order_id ) {
        return;
    }

    $order = wc_get_order( $order_id );
    $order->update_status( 'completed' );
}

/**
 * Check if Conditional Product is In cart
 *
 * @param $product_id
 *
 * @return bool
 */
function wordimpress_is_conditional_product_in_cart( $product_id ) {
//Check to see if user has product in cart
global $woocommerce;

//flag no book in cart
$book_in_cart = false;

foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
    $_product = $values['data'];
        $terms = get_the_terms( $_product->id, 'product_cat' );

            foreach ($terms as $term) {
                $_categoryid = $term->term_id;
            }

    if ( $_categoryid === 21 ) {
        //book is in cart!
        $book_in_cart = true;
    }

}

return $book_in_cart;

}
//add_action( 'woocommerce_before_cart', 'bbloomer_find_product_in_cart' );
  
function bbloomer_find_product_in_cart() {

	$product_id = 89;

	$product_cart_id = WC()->cart->generate_cart_id( $product_id );
	$in_cart = WC()->cart->find_product_in_cart( $product_cart_id );

	if ( $in_cart ) {

		$notice = 'Product ID ' . $product_id . ' is in the Cart!';
		wc_print_notice( $notice, 'notice' );
		echo "Hello World";

	}

}

// unset checkout field
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
  
function custom_override_checkout_fields( $fields ) {
  // remove billing fields
    unset($fields['billing']['billing_first_name']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
    unset($fields['billing']['billing_email']);

 // remove shipping fields 
    unset($fields['shipping']['shipping_first_name']);    
    unset($fields['shipping']['shipping_last_name']);  
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_address_1']);
    unset($fields['shipping']['shipping_address_2']);
    unset($fields['shipping']['shipping_city']);
    unset($fields['shipping']['shipping_postcode']);
    unset($fields['shipping']['shipping_country']);
    unset($fields['shipping']['shipping_state']);
	// remove order comment fields
    unset($fields['order']['order_comments']);
	/*
	$fields['order']['order_comments']['placeholder'] = 'Please type your PO number here and we will add it to the invoice.';
     $fields['order']['order_comments']['label'] = '';
	*/
	$fields['billing']['billing_first_name'] = array(
    'label'     => __('', 'woocommerce'),
    'placeholder'   => _x('Name', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true,
	'priority'  => 10
     );
	
	 $fields['billing']['billing_company'] = array(
        'label'     => __('', 'woocommerce'),
    'placeholder'   => _x('Company', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true,
	'priority'  => 20
     );
	$fields['billing']['billing_phone'] = array(
        'label'     => __('', 'woocommerce'),
    'placeholder'   => _x('Phone', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true,
	'priority'  => 30
     );
	 $fields['billing']['billing_email'] = array(
    'label'     => __('', 'woocommerce'),
    'placeholder'   => _x('Email', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true,
	'priority'  => 40
     );
	 // Custtom Field Add
	/*$fields['billing']['your_field']['options'] = array(
  'option_1' => 'Option 1 text',
  'option_2' => 'Option 2 text'
); */
	 

	
    return $fields;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );


/**
 * Add the field to the checkout
 */
add_action( 'woocommerce_after_order_notes', 'industry_checkout_field' );

function industry_checkout_field( $checkout ) {

   // echo '<div id="industry_checkout_field"><h2>' . __('My Field') . '</h2>';

    woocommerce_form_field( 'industry_name', array(
        'type'          => 'text',
        'class'         => array('industry-class form-row-wide'),
        'label'         => __(''),
		'required'         => true,
        'placeholder'   => __('Industry'),
        ), $checkout->get_value( 'industry_name' ));

    echo '</div>';

}


//* Add select field to the checkout page
//add_action('woocommerce_before_order_notes', 'wps_add_select_checkout_field');
function wps_add_select_checkout_field( $checkout ) {
	echo '<h2>'.__('Next Day Delivery').'</h2>';
	woocommerce_form_field( 'daypart', array(
	    'type'          => 'select',
	    'class'         => array( 'wps-drop' ),
	    'label'         => __( 'Delivery options' ),
	    'options'       => array(
	    	'blank'		=> __( 'Select a day part', 'wps' ),
	        'morning'	=> __( 'In the morning', 'wps' ),
	        'afternoon'	=> __( 'In the afternoon', 'wps' ),
	        'evening' 	=> __( 'In the evening', 'wps' )
	    )
 ),
	$checkout->get_value( 'daypart' ));
}
//* Process the checkout
 //add_action('woocommerce_checkout_process', 'wps_select_checkout_field_process');
 function wps_select_checkout_field_process() {
    global $woocommerce;
    // Check if set, if its not set add an error.
    if ($_POST['daypart'] == "blank")
     wc_add_notice( '<strong>Please select a day part under Delivery options</strong>', 'error' );
 }
 //* Update the order meta with field value
 add_action('woocommerce_checkout_update_order_meta', 'wps_select_checkout_field_update_order_meta');
 function wps_select_checkout_field_update_order_meta( $order_id ) {
   if ($_POST['daypart']) update_post_meta( $order_id, 'daypart', esc_attr($_POST['daypart']));
 }
 //* Display field value on the order edition page
//add_action( 'woocommerce_admin_order_data_after_billing_address', 'wps_select_checkout_field_display_admin_order_meta', 10, 1 );
function wps_select_checkout_field_display_admin_order_meta($order){
	echo '<p><strong>'.__('Delivery option').':</strong> ' . get_post_meta( $order->id, 'daypart', true ) . '</p>';
}
//* Add selection field value to emails
//add_filter('woocommerce_email_order_meta_keys', 'wps_select_order_meta_keys');
function wps_select_order_meta_keys( $keys ) {
	$keys['Daypart:'] = 'daypart';
	return $keys;
	
}
// checkout biiling and shipping heading change
function wc_billing_field_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Billing details' :
            $translated_text = __( 'Provide Your Contact Information', 'woocommerce' );
            break;
		 case 'Additional information' :
            $translated_text = __( 'Fill Out the Creative Brief', 'woocommerce' );
            break;
    }
    return $translated_text;
}
add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );
/* Product Tab
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

    $tabs['additional_information']['title'] = __( 'Additional Information' );  // Rename the Additional Information text
    return $tabs;

}
*/

site_url();
/**
 * Changes the redirect URL for the Return To Shop button in the cart.
 */
function wc_empty_cart_redirect_url() {
  return 'http://example.url/category/specials/';
}
//add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url', 10 );
?> 